{{- if .Values.apiserver.enableValidatingWebhook }}
{{- if not (list "kubedb-autoscaler" "kubedb-dashboard" "kubedb-ops-manager" "kubedb-schema-manager" | has .Values.server.repository) }}

{{- $caCrt := dig "data" "ca.crt" "unknown" (lookup "v1" "ConfigMap" .Release.Namespace "kube-root-ca.crt") | b64enc }}

apiVersion: admissionregistration.k8s.io/v1
kind: ValidatingWebhookConfiguration
metadata:
  name: validators.kubedb.com
  labels:
    app.kubernetes.io/component: kubedb-provisioner
    {{- include "kubedb-webhook-server.labels" . | nindent 4 }}
webhooks:
- name: elasticsearchwebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/elasticsearchwebhooks
    caBundle: {{ $caCrt }}
  rules:
  - apiGroups: ["kubedb.com"]
    apiVersions: ["*"]
    resources: ["elasticsearches"]
    operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: postgreswebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/postgreswebhooks
    caBundle: {{ $caCrt }}
  rules:
  - apiGroups: ["kubedb.com"]
    apiVersions: ["*"]
    resources: ["postgreses"]
    operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: mysqlwebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/mysqlwebhooks
    caBundle: {{ $caCrt }}
  rules:
  - apiGroups: ["kubedb.com"]
    apiVersions: ["*"]
    resources: ["mysqls"]
    operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: perconaxtradbwebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/perconaxtradbwebhooks
    caBundle: {{ $caCrt }}
  rules:
  - apiGroups: ["kubedb.com"]
    apiVersions: ["*"]
    resources: ["perconaxtradbs"]
    operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: mongodbwebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/mongodbwebhooks
    caBundle: {{ $caCrt }}
  rules:
  - apiGroups: ["kubedb.com"]
    apiVersions: ["*"]
    resources: ["mongodbs"]
    operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: rediswebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/rediswebhooks
    caBundle: {{ $caCrt }}
  rules:
  - apiGroups: ["kubedb.com"]
    apiVersions: ["*"]
    resources: ["redises"]
    operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: redissentinelwebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/redissentinelwebhooks
    caBundle: {{ $caCrt }}
  rules:
    - apiGroups: ["kubedb.com"]
      apiVersions: ["*"]
      resources: ["redissentinels"]
      operations: ["CREATE", "UPDATE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: memcachedwebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/memcachedwebhooks
    caBundle: {{ $caCrt }}
  rules:
  - apiGroups: ["kubedb.com"]
    apiVersions: ["*"]
    resources: ["memcacheds"]
    operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: etcdwebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/etcdwebhooks
    caBundle: {{ $caCrt }}
  rules:
  - apiGroups: ["kubedb.com"]
    apiVersions: ["*"]
    resources: ["etcds"]
    operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: pgbouncerwebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/pgbouncerwebhooks
    caBundle: {{ $caCrt }}
  rules:
  - apiGroups: ["kubedb.com"]
    apiVersions: ["*"]
    resources: ["pgbouncers"]
    operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: proxysqlwebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/proxysqlwebhooks
    caBundle: {{ $caCrt }}
  rules:
  - apiGroups: ["kubedb.com"]
    apiVersions: ["*"]
    resources: ["proxysqls"]
    operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: namespacewebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/namespacewebhooks
    caBundle: {{ $caCrt }}
  rules:
  - apiGroups: [""]
    apiVersions: ["*"]
    resources: ["namespaces"]
    operations: ["DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: Ignore
  sideEffects: None
- name: mariadbwebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/mariadbwebhooks
    caBundle: {{ $caCrt }}
  rules:
    - apiGroups: ["kubedb.com"]
      apiVersions: ["*"]
      resources: ["mariadbs"]
      operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
- name: kafkawebhook.validators.kubedb.com
  clientConfig:
    service:
      namespace: default
      name: kubernetes
      path: /apis/validators.kubedb.com/v1alpha1/kafkawebhooks
    caBundle: {{ $caCrt }}
  rules:
    - apiGroups: ["kubedb.com"]
      apiVersions: ["*"]
      resources: ["kafkas"]
      operations: ["CREATE", "UPDATE", "DELETE"]
  admissionReviewVersions: ["v1beta1"]
  failurePolicy: {{ .Values.apiserver.webhook.failurePolicy }}
  sideEffects: None
{{- end }}
{{- end }}
